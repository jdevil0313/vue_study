1. npm i vue-style-loader -D
2. npm i css-loader -D
3. webpack.config.js 파일에서 모듈 룰(vue-style-loader, css-loader)을  아래 처럼 추가해준다.

module: {
    rules:[{
        test: /\.vue$/,
        loader: 'vue-loader', //use: 'vue-loader' 이렇게 해도 된다.
    }, {
        test : /\.css$/,
        use: [
            'vue-style-loader',
            'css-loader'
        ]
    }]
},