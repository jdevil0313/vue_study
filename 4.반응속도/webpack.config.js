const VueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path'); //절대 경로를 써야 하는데 node에서 제공하는 스크립트 아래 output path 에서 path.join(__dirname, 'dist')로 쓸 수 있다. 'c:/경로/경로/dist'(./dist) 와 같은 의미이다

module.exports = {
    mode: 'development', //'development'(개발) //mode: 'production'(실제사용)
    devtool: 'eval', //'eval'(개발) , 'hidden-source-map'(상용)
    resolve: {
        extensions: ['.js', '.vue'] //확장자 처리 main.js 파일에서 import NumberBaseball from './NumberBaseball.js'; 에서 .js 파일명을 생략할 수 있다.
    },
    entry: {
        app: path.join(__dirname, 'main.js'),
    },
    module: {
        rules:[{
            test: /\.vue$/,
            loader: 'vue-loader', //use: 'vue-loader' 이렇게 해도 된다.
        }, {
            test : /\.css$/,
            use: [
                'vue-style-loader',
                'css-loader'
            ]
        }]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    output: {
        filename: '[name].js', //app.js 로 해도 됨
        path: path.join(__dirname, 'dist'),
        publicPath: '/dist',
    }
}