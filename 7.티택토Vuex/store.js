import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex); //vue와 vuex를 연결하는 작업 this.$store 가 생성됨

export const SET_WINNER = 'SET_WINNER'; //import { SET_WINNER, CLICK_CELL, CHANGE_TURN } from './store';
export const CLICK_CELL = 'CLICK_CELL';
export const CHANGE_TURN = 'CHANGE_TURN';
export const RESET_GAME = 'RESET_GAME';
export const NO_WINNER = 'NO_WINNER';

export default new Vuex.Store({ //import store from './store';
    state: { //vu의 data와 비슷
        tableData: [
            ['', '', ''], 
            ['', '', ''], 
            ['', '', '']
        ],
        turn: 'O',
        winner: ''
    }, 
    getters: { //vue의 computed와 비슷
        turnMessage(state) {
            return this.turn + '님이 승리 하셨습니다.';
        }
    },
    mutations: { //state를 수정할때 사용. 동기적
        [SET_WINNER](state, winner) {
            state.winner = winner;
        },
        [CLICK_CELL](state, { row, cell }) {
            Vue.set(state.tableData[row], cell, state.turn);
        },
        [CHANGE_TURN](state) {
            state.turn = state.turn === 'O' ? 'X' : 'O';
        },
        [RESET_GAME](state) {
            state.turn = 'O';
            state.tableData = [
                ['', '', ''], 
                ['', '', ''], 
                ['', '', '']
            ];
        },
        [NO_WINNER](state) {
            state.winner = '';
        }
    },
    actions: { //비동기를 사용할 때, 또는 여러 뮤테이션을 연달아 실행할 때

    }
});