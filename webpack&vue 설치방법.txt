webpack 사용방법
1. node.js 사이트에서 lts 안전된 버전 다운로드
2. 설치 후 node -v 와 npm -v 로 버전확인 후 제대로 설치 됬는지 확인
3. 프로젝트 위치에서 npm init (엔터 후) package name : 나오는데 페키지명(예: number-baseball)을 적은 후 이후에 나오는 질문에는 모두 엔터 한다.
4. npm init 이후에 아래 내용처럼 package.json 파일이 생성된다.
<package.json 파일>
{
  "name": "number-baseball",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "build": "webpack"
  },
  "author": "",
  "license": "ISC",
  "dependencies": {
    "vue": "^2.6.11"
  },
  "devDependencies": {
    "vue-loader": "^15.9.1",
    "vue-template-compiler": "^2.6.11",
    "webpack": "^4.42.0",
    "webpack-cli": "^3.3.11"
  }
}
//vue, vue-template-compiler 버전은 항상 같아야 한다.
//특정 버전을 깔고 싶으면 npm i vue@2.7.0

5. npm install vue(뷰 설치) 후
"dependencies": {
  "vue": "^2.6.11"
}
내용이 추가 된다.
6. npm i(install를 i롤 줄여서 사용할 수 있다) webpack webpack-cli -D(webpack이랑 webpack-cli 개발할 때만 사용하겠다. 라는 옵션) 아래 처럼 package.json 파일에 devDependencies 가 추가 된다.
"devDependencies": {
  "vue-loader": "^15.9.1",
  "vue-template-compiler": "^2.6.11",
  "webpack": "^4.42.0",
  "webpack-cli": "^3.3.11"
}
7. 현 프로젝트 webpack.config.js 파일을 만들어 준다. webpack 관련한 설정에 대해서 적어준다.