const VueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path');

module.exports = {
    mode: 'development', //'development'(개발) //mode: 'production'(실제사용)
    devtool: 'eval', //'eval'(개발) , 'hidden-source-map'(상용)
    resolve: {
        extensions: ['.js', '.vue'] //확장자 처리 
    },
    entry: {
        app: path.join(__dirname, 'main.js'),
    },
    module: {
        rules:[{
            test: /\.vue$/,
            loader: 'vue-loader', //use: 'vue-loader' 이렇게 해도 된다.
        }]
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    output: {
        filename: '[name].js', //app.js 로 해도 됨
        path: path.join(__dirname, 'dist'),
    }
}